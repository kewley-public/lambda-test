provider "aws" {
  max_retries = 2
  region = "${var.region}"
  profile = "${var.profile}"
}

terraform {
  required_version = ">= 0.11.8"
  backend "s3" {
    bucket = "kewley-terraform"
    key = "test-api/terraform.tfstate"
    region = "us-east-1"
  }
}

module "api" {
  source = "./lambda"
  app_name = "${var.app_name}"
  environment = "${var.environment}"
  s3_bucket_name = "${var.s3_bucket_name}"
  s3_api_key_map = "${var.s3_api_key_map}"
  subnets = "${var.subnets}"
  vpc_id = "${var.vpc_id}"
}
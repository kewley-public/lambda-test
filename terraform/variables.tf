# ---------------------------------------------------------------------------------------------------------------------
# Main application parameters required
# ---------------------------------------------------------------------------------------------------------------------
variable "region" {
  description = "The region to install our terraform resources"
  default = "us-east-1"
}

variable "profile" {
  description = "The profile to use for terraform"
  default = "default"
}

variable "app_name" {
  description = "The application name"
  default = "test-app"
}

variable "environment" {
  description = "The environment"
  default = "dev"
}

variable "s3_bucket_name" {
  description = "The S3 bucket name which holds onto our API"
}

variable "s3_api_key_map" {
  description = "The API keys"
  type = "map"
}

variable "vpc_id" {
  description = "The VPC ID"
}

variable "subnets" {
  description = "The subnet id's for the lambda functions"
  type = "list"
}

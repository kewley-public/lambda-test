import json
import traceback
import uuid

from .logger import get_logger

logger = get_logger()


def process_request(event, api_method):
    """
    Helper to process requests and handle exception-handling and other boilerplate.
    Exceptions/validation can still be handled separately within individual lambdas, but
    this covers the basics that we want applied to all.
    :param event: The lambda event passed in from API Gateway
    :param api_method: The function to execute the event
    :return: API Gateway proxy response
    """
    try:
        response = api_method(event)
    except Exception as e:
        if 'requestContext' in event and 'authorizer' in event['requestContext']:
            principal_id = event['requestContext']['authorizer']['principalId']
        else:
            principal_id = ""
        logger.error("Unhandled exception occurred for user %s:\n"
                     "Type/message: %s\n"
                     "Stack trace: %s"
                     % (principal_id, repr(e), traceback.format_exc()))

        response = {
            "statusCode": 500,
            "headers": {"Cache-Control": "no-cache, no-store, must-revalidate, max-age=0"},
            "body": json.dumps({
                "errorMessage": repr(e)
            })
        }
    return response


def generate_uuid():
    return str(uuid.uuid4())
